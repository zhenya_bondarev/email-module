import { Component, OnInit, OnDestroy } from "@angular/core";
import { EmailService, EmailUserAssignmentService, AppState } from "../../common/common.services";
import { Email, EmailStatus, APP_STATE_ATTRIBUTES } from "../../common/common.typings";
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'email',
  templateUrl: 'email.component.html',
  styleUrls: [ './email.component.scss' ],
})
export class EmailComponent implements OnInit, OnDestroy {
  email: Email;

  initialized: boolean = false;
  EmailStatus = EmailStatus;

  emailUsersAssign: {value:number, label: string}[] = [];
  emailUserAssignId: number;

  private activateRouteSub: any;
  private tagId: string;

  constructor(
    public appState: AppState,
    private api: EmailService,
    private apiAssigned: EmailUserAssignmentService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
  }

  private _htmlBody;
  get htmlBody() {
    return this._htmlBody;
  }
  set htmlBody(value: string) {
    this._htmlBody = this.sanitizer.bypassSecurityTrustHtml(value);
  }

  ngOnInit() {
    this.activateRouteSub = this.route.data
      .subscribe(({env}) => {
        this.tagId = env.tag;

        this.restoreFromState();
        if (this.email && this.email.IsFullDownload) {
          this.htmlBody = this.email.HtmlBody;
          this.initialized = true;
          return;
        }

        this.api.get(env.mail)
          .subscribe((data: Email | null) => {
            if (data) {
              if (!this.email) {
                this.email = data;
              }

              this.email.HtmlBody = data.HtmlBody;
              this.email.Attachments =  data.Attachments;
              this.htmlBody = data.HtmlBody;

              this.saveToState();
              this.onReadEmail();
              this.initialized = true;
            }
          }, () => {
            this.initialized = true;
          });

        this.apiAssigned.all({mail: env.mail}).subscribe(data => {
          data.forEach((model: EmailUserAssigned) => {
            this.emailUsersAssign.push({
              value: model.Id,
              label: model.Fullname
            });
          })
        });
      });
  }

  ngOnDestroy() {
    this.activateRouteSub.unsubscribe();
  }

  //CUSTOM EVENTS
  onToggleImportant() {
    this.email.IsImportant = !this.email.IsImportant;
  }

  onBack() {
    this.router.navigate(['/tag', this.tagId]);
  }

  onReadEmail() {
    if (!this.email.IsNew) {
      return;
    }

    this.onChangeStatus(EmailStatus.READ);
  }

  onChangeStatus(status: EmailStatus) {
    this.updateEmail({Status: status});
  }

  onAssignUser({label, value}) {
    this.emailUserAssignId = value;
  }

  onApplyUser() {
    if (this.emailUserAssignId == 0) {
      return;
    }

    this
      .updateEmail({AssignedTo: this.emailUserAssignId})
      .then(() => {
        this.onBack();
      });
  }

  onDeselectUser() {
    this.emailUserAssignId = 0;
  }

  private updateEmail({
    Status = this.email.Status,
    AssignedTo = this.email.AssignedTo
  }) {
    return new Promise((resolve, reject) => {
      let oldStatus = this.email.Status;
      let oldAssignedTo = this.email.AssignedTo;

      this.email.Status = Status;
      this.email.AssignedTo = AssignedTo;

      this.api.update(this.email)
        .subscribe((result) => {
          resolve(result);

        }, error => {
          reject(error);
          this.email.Status = oldStatus;
          this.email.AssignedTo = oldAssignedTo;
        });
    });
  }

  private restoreFromState() {
    if (!this.appState.has(APP_STATE_ATTRIBUTES.CURRENT_EMAIL)) {
      return;
    }

    this.email = this.appState.get(APP_STATE_ATTRIBUTES.CURRENT_EMAIL);
  }

  private saveToState() {
    this.appState.set(APP_STATE_ATTRIBUTES.CURRENT_EMAIL, this.email);
  }
}
