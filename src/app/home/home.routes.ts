import { Routes } from '@angular/router';
import { EmailListComponent } from './+email-list';
import { EmailComponent } from './+email';
import { HomeComponent } from "./home.component";
import { HomeDetailResolver } from "./home.resolver";

export const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'tag/:tagId/:mailId',
        component: EmailComponent,
        resolve: {
          env: HomeDetailResolver
        }
      },
      {
        path: 'tag/:tagId',
        component: EmailListComponent,
        resolve: {
          env: HomeDetailResolver
        }
      },
      {
        path: '',   redirectTo: 'tag/inbox', pathMatch: 'full'
      },
    ]
  }
];
