import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../common/common.services';

@Component({
  selector: 'home',  // <home></home>
  styleUrls: [ './home.component.scss' ],
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
  // Set our default values
  public localState = { value: '' };

  constructor(
    public appState: AppState,
  ) {}

  public ngOnInit() {
  }

  public submitState(value: string) {
    this.appState.set('value', value);
    this.localState.value = '';
  }
}
