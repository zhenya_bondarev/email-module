///<reference path="../../../../node_modules/rxjs/add/observable/from.d.ts"/>
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";

import { EmailService, AppState } from "../../common/common.services";
import { Email, EMAIL_DEFAUL_TAG_NONE, APP_STATE_ATTRIBUTES } from "../../common/common.typings";


@Component({
  selector: 'email-list',
  styleUrls: [ './email-list.component.scss' ],
  templateUrl: 'email-list.component.html'
})
export class EmailListComponent implements OnInit, OnDestroy {
  emails: Email[] = [];
  initialized: boolean = false;
  isAllEmailChecked: boolean = false;

  private activateRouteSub: any;
  private tagId: string | null;
  offset: number = 0;
  limit: number = 5;

  constructor(
    public appState: AppState,
    private api: EmailService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.activateRouteSub = this.activateRoute.data
      .subscribe(({env}) => {
        this.tagId = env.tag;

        if (this.appState.has(APP_STATE_ATTRIBUTES.EMAILS)) {
          if (this.restoreFromState(this.appState.get(APP_STATE_ATTRIBUTES.EMAILS))) {
            this.initialized = true;
            return;
          }
        }

        let params: ApiRequestMail = {};
        if (env.tag !== EMAIL_DEFAUL_TAG_NONE) {
          params.tag = this.tagId;
        }
        params.offset = this.offset;
        params.limit = this.limit;

        this.api.all(params)
          .subscribe((emails: Email[]) => {
            this.emails = emails;
            this.initialized = true;

            this.saveToSate();
          });
      });
  }

  ngOnDestroy() {
    this.activateRouteSub.unsubscribe();
  }

  // custom events
  onToggleImportant(email: Email) {
    email.IsImportant = !email.IsImportant;
  }

  onCheckEmail(email: Email) {
    email.IsChecked = !email.IsChecked;
    Observable
      .from(this.emails)
      .reduce((prevValue, email: Email) => {
        if (email.IsChecked) {
          return ++prevValue;
        }

        return prevValue;
      }, 0)
      .subscribe(number => {
        this.isAllEmailChecked = this.emails.length === number;
      });
  }

  onFilterTag(tag: Tag) {
    this.router.navigate(['/tag', tag.Id]);
  }

  onViewEmail(email: Email) {
    this.appState.set(APP_STATE_ATTRIBUTES.CURRENT_EMAIL, email);
    this.router.navigate(['/tag', this.tagId, email.Id]);
  }

  onCheckAllEmail() {
    this.isAllEmailChecked = !this.isAllEmailChecked;
    Observable.from(this.emails).forEach((email: Email) => {
      email.IsChecked = this.isAllEmailChecked;
    });
  }

  private saveToSate() {
    let emailsState: EmailPagination = {
      emails: this.emails,
      offset: this.offset,
      limit: this.limit,
      tag: this.tagId
    };

    this.appState.set(APP_STATE_ATTRIBUTES.EMAILS, emailsState);
  }

  private restoreFromState(state: EmailPagination): boolean {
    if (this.tagId != state.tag) {
      return false;
    }

    this.emails = state.emails;
    this.offset = state.offset;
    this.limit = state.limit;
    this.tagId = state.tag;

    return true;
  }
}
