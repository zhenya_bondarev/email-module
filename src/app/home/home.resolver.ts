import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from "rxjs";

import { EMAIL_DEFAUL_TAG_NONE } from "../common/common.typings";

@Injectable()
export class HomeDetailResolver implements Resolve<EmailResolverData> {
  constructor(private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return Observable.of({
      tag: route.params['tagId'] ? route.params['tagId'] : EMAIL_DEFAUL_TAG_NONE,
      mail: route.params['mailId']
    });
  }
}
