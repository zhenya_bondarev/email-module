import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '../common/common.module'
import { CommonModule } from '@angular/common';
import { SelectModule } from 'angular2-select';

import { ROUTES } from './home.routes';
import { HomeComponent } from './home.component';
import { EmailService } from '../common/common.services';
import { EmailListComponent } from './+email-list/email-list.component';
import { EmailComponent } from './+email/email.component';
import { SidebarComponent } from "./sidebar/sidebar.component";
import { HeaderComponent } from './header/header.component';
import { FormsModule } from "@angular/forms";
import { HomeDetailResolver } from "./home.resolver";

@NgModule({
    imports: [
      CommonModule,
      AppCommonModule,
      FormsModule,
      SelectModule,
      RouterModule.forChild(ROUTES),
    ],
    exports: [],
    declarations: [
      HomeComponent,
      EmailListComponent,
      EmailComponent,
      SidebarComponent,
      HeaderComponent
    ],
    providers: [
      EmailService,
      HomeDetailResolver
    ],
})
export class HomeModule { }
