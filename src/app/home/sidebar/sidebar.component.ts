import { Component, OnInit } from "@angular/core";

import { TagService } from '../../common/services/tags.service';

@Component({
  selector: 'home-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  tags: Tag[] = [];
  initialized = false;

  constructor(private api: TagService) {}

  ngOnInit() {
    this.api.all()
      .subscribe((tags: Tag[]) => {
        this.tags = tags;
        this.initialized = true;
      });
  }
}
