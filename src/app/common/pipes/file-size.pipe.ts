import { Pipe, PipeTransform } from '@angular/core';

/*
 * Convert bytes into largest possible unit.
 * Takes an precision argument that defaults to 2.
 * Usage:
 *   bytes | fileSize:precision
 * Example:
 *   {{ 1024 |  fileSize}}
 *   formats to: 1 KB
 */
@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {
  static K = 1024;
  private static units = [
    'b',
    'Kb',
    'Mb',
    'Gb',
    'Tb',
    'Pb',
    'Eb',
    'Zb',
    'Yb'
  ];

  transform(bytes: number = 0, precision: number = 2 ) : string {
    if(bytes == 0) {
      return '0 ' + FileSizePipe.units[0];
    }
    let i = Math.floor(Math.log(bytes) / Math.log(FileSizePipe.K));

    return parseFloat((bytes / Math.pow(FileSizePipe.K, i)).toFixed(precision)) + ' ' + FileSizePipe.units[i];
  }
}
