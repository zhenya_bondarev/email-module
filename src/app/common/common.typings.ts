export const EMAIL_DEFAUL_TAG_NONE = 'inbox';
export const APP_STATE_ATTRIBUTES = {
  EMAILS: 'emails',
  CURRENT_EMAIL: 'current_email',
};

export enum EmailStatus {
  NEW_MESSAGE,
  ASSIGNED,
  READ,
  HANDLING = 10,
  WAIT_FOR_INFORMATION,
  WAIT_FOR_APPROVAL,
  HANDLED = 20,
  ARCHIVED = 30,
  DELETED
}

export class Email implements EmailModelApi {
  Id: string;
  Sender: string;
  Subject: string;
  Text: string;
  Size: number;
  Recipient: string;
  Flags: string;
  Tags: EmailTag[];
  Attachments: EmailAttachment[];
  HtmlBody: string;
  Status: EmailStatus;
  AssignedTo: number;

  IsImportant: boolean;
  IsChecked: boolean;

  private _received?: Date;
  private static ATTRIBUTES_TO_SAVE = ['Id', 'Status', 'AssignedTo'];

  get IsRead() {
    return this.Status == EmailStatus.READ;
  };
  get IsNew() {
    return this.Status == EmailStatus.ASSIGNED
      || this.Status == EmailStatus.NEW_MESSAGE;
  }
  get IsEnableToHandling() {
    return this.Status < EmailStatus.HANDLING;
  }
  get IsHandling() {
    return this.Status >= EmailStatus.HANDLING && this.Status < EmailStatus.HANDLED;
  }
  get IsHandled() {
    return this.Status == EmailStatus.HANDLED;
  }

  get dateObject(): Date {
    return this._received;
  }

  get dateAsUnixtime(): number {
    return this._received ?
      +(this._received.getTime() / 1000).toFixed(0) : 0;
  }

  get received(): string {
    return this.dateObject.toString();
  }

  set received(time: string) {
    this._received = new Date(time);
    if (this._received.toString() === 'Invalid Date') {
      this._received = undefined;
    }
  }
  get IsFullDownload() {
    return !!this.HtmlBody;
  }

  constructor(source?: EmailModelApi) {
    Object.assign(this, source ? source : {});
  }

  toSave() {
    let toSaveObject = {};
    Email.ATTRIBUTES_TO_SAVE.forEach(name => {
      toSaveObject[name] = this[name];
    });

     return toSaveObject;
  }
}
