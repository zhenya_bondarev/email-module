import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { EmailService, ApiService, TagService, EmailUserAssignmentService } from './common.services';
import { FileSizePipe } from './common.pipes';

const PIPES = [FileSizePipe];
const SERVICES = [EmailService, ApiService, TagService, EmailUserAssignmentService];

@NgModule({
    imports: [
      HttpModule,
    ],
    exports: [
      HttpModule,
      ...PIPES
    ],
    declarations: [
      ...PIPES,
    ],
    providers: [
      ...SERVICES
    ],
})
export class AppCommonModule { }
