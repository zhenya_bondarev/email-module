import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Response } from "@angular/http";

import { ApiService } from "./api.service";

//TODO pagination
@Injectable()
export class EmailUserAssignmentService {
  private _users = [];

  constructor(private api: ApiService) {}

  all(params?: ApiRequestMailUser): Observable<EmailUserAssigned[]> {
    let search = ApiService.getURLSearchParams(params);

    if (this._users.length) {
      return Observable.of(this._users);
    }

    return this.api.http
      .get(this.api.getEndpoint('user'), this.api.getOptions(search))
      .timeout(ApiService.TIMEOUT)
      .map((res: Response) => {
        let data: ResponseCollection = this.api.extractData(res);
        this._users = data.Data;
        return this._users;
      })
      .catch(err => {
        return Observable.of<EmailUserAssigned[]>([]);
      });
  }
}
