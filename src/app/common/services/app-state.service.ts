import { Injectable } from '@angular/core';
import { Map } from 'immutable';

@Injectable()
export class AppState {

  public _state: Map<string, any> = Map.of();

  public get state() {
    return this._state;
  }

  public set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }

  public get(prop: any, notSetValue = null) {
    return this._state.get(prop, notSetValue);
  }

  public has(prop: any) {
    return this._state.has(prop);
  }

  public set(prop: string, value: any) {
    this._state = this._state.set(prop, value);
  }
}
