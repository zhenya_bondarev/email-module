import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Response, URLSearchParams } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

import { ApiService } from "./api.service";
import { Email } from "../common.typings";

//TODO pagination
@Injectable()
export class EmailService {
  static EMAIL_CACHE_LENGTH = 10;

  private _emailsCache: Email[] = [];

  constructor(private api: ApiService) {}

  all(params?: ApiRequestMail): Observable<Email[]> {
    let search = ApiService.getURLSearchParams(params);

    return this.api.http
      .get(this.api.getEndpoint('mail'), this.api.getOptions(search))
      .map((res: Response) => {
        let data: ResponseCollection = this.api.extractData(res);
        if (data.Data) {
          return data.Data.map((model: EmailModelApi) => new Email(model));
        }

        throw new Error('Emails not received');
      })
      .timeout(ApiService.TIMEOUT)
      .catch(err => {
        return Observable.of<Email[]>([])
      });
  }

  get(id: string) {
    let model = this.getEmailFormCache(id);
    if (model !== null) {
      return Observable.of<Email>(model);
    }

    return this.api.http
      .get(this.api.getEndpoint('mail/' + id), this.api.getOptions())
      .timeout(ApiService.TIMEOUT)
      .map((res: Response) => {
        if (this._emailsCache && this._emailsCache.length >= EmailService.EMAIL_CACHE_LENGTH) {
          Array.prototype.splice.call(this._emailsCache, 0, 1);
        }

        let model = this.api.extractData(res);
        if (!model) {
          throw new Error('Was not received');
        }

        let modelEmail = new Email(model);
        this._emailsCache.push(modelEmail);
        return modelEmail;
      })
      .catch(err => {
        return Observable.of(null)
      });
  }

  update(email: Email) {
    return this.api.http
      .put(this.api.getEndpoint('mail/' + email.Id), JSON.stringify(email.toSave()), this.api.getOptions())
      .timeout(ApiService.TIMEOUT);
  }

  private getEmailFormCache(id: string) {
    let index = -1;
    this._emailsCache.forEach((email: Email, i: number) => {
      if (email.Id === id) {
        index = i;
        return;
      }
    });

    if (index >= 0) {
      return this._emailsCache[index];
    }

    return null;
  }
}
