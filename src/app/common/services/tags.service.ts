import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Response } from "@angular/http";

import { ApiService } from "./api.service";

//TODO pagination
@Injectable()
export class TagService {
    private _tags = [];

    constructor(private api: ApiService) {}

    all(): Observable<Tag[]> {
      if (this._tags.length) {
        return Observable.of(this._tags);
      }

      return this.api.http
        .get(this.api.getEndpoint('tags'), this.api.getOptions())
        .timeout(ApiService.TIMEOUT)
        .map((res: Response) => {
          let data: ResponseCollection = this.api.extractData(res);
          this._tags = data.Data;
          return this._tags;
        })
        .catch(err => {
          return Observable.of<Tag[]>([])
        });
    }
}
