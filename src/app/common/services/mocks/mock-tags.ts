export const TAGS_RESPONSE = {
  "Data": [
    {
      "Id": "2ecc6540-0f69-4595-a3bf-561093499bcf",
      "Name": "Some Test Tag 1",
      "TagType": 0
    },
    {
      "Id": "80834cfb-a18e-4b80-8efe-d4ad907246a9",
      "Name": "Some Test Tag 2",
      "TagType": 1
    },
    {
      "Id": "cc197f6e-190c-45fc-b5ba-d8a59e96ea9d",
      "Name": "Some Test Tag 3",
      "TagType": 2
    }
  ],
  "Count": 3,
  "PageSize": 3
};
