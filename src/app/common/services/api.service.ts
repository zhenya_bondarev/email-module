import { Headers, RequestOptions, Response, Http, URLSearchParams, RequestOptionsArgs } from '@angular/http';
import { Injectable } from '@angular/core';
import { API } from '../config';

@Injectable()
export class ApiService {
  static TIMEOUT = 20000;

  private _options: RequestOptions;

  private apiEnter: string;

  get http(): Http {
    return this._http;
  }

  constructor(
    private _http: Http
  ) {
    this.apiEnter = API.ENTRY;
    let innerHeaders = new Headers();
    innerHeaders.append('content-type', 'application/json');

    this._options = new RequestOptions({
      // withCredentials: true,
      headers: innerHeaders
    });
    if (this.apiEnter.lastIndexOf('/') !== this.apiEnter.length - 1) {
      this.apiEnter += '/';
    }
  }

  public getEndpoint(path: string) {
    return this.apiEnter + path;
  }

  public extractData(res: Response): any {
    let body = res.json();

    return body || {};
  }

  public getOptions(search?: string | URLSearchParams) {
    return this._options.merge({search});
  }

  static getURLSearchParams(params?: Object): URLSearchParams {
    let urlParams = new URLSearchParams();
    if (params) {
      Object.keys(params).forEach(name => {
        urlParams.set(name, params[name]);
      });
    }

    return urlParams;
  }
}
