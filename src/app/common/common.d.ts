declare const EMAIL_FLAG_DELIMITER = ' ';

declare interface InternalStateType {
  [key: string]: any;
}

declare interface ResponseCollection {
  Data: any[];
  Count: number;
  PageSize: number;
}

declare interface StoreType {
  state: InternalStateType;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
}

declare interface Tag {
  Id: string;
  Name: string;
  TagType: number;
}

declare interface EmailAttachment {
  Index: number | null;
  Mine: string;
  Name: string;
  Size: number;
}

declare interface EmailTag {
  Id: string;
  Name: string;
  TagType: number;
}

declare interface ApiRequestPagination {
  offset?: number;
  limit?: number;
}

declare interface ApiRequestMail {
  tag?: string;
  offset?: number;
  limit?: number;
}

declare interface ApiRequestMailUser {
  mail: string;
}

declare interface EmailModelApi {
  Id: string;
  Sender: string;
  Subject: string;
  Text: string;
  Size: number;
  Recipient: string;
  Flags: string;
  Attachments: EmailAttachment[];
  Status: Number;
}

declare interface EmailResolverData {
  tag: string;
}

declare interface EmailUserAssigned {
  Id: number;
  Username: string;
  Fullname: string;
}

declare interface EmailPagination {
  emails: any[];
  tag: string;
  offset: number;
  limit: number;
}
